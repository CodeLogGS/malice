#!/usr/bin/env python

"""
Malice: A simple interpreted programming language.
    Copyright (C) 2017  Jaco Malan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sys import *
import numpy as np

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def printError(error_code):
    if error_code == 100:
        print(bcolors.FAIL + "Error code 100." + bcolors.ENDC)
    elif error_code == 110:
        print(bcolors.FAIL + "Error: found unknown type. (Error code 110)" + bcolors.ENDC)
    elif error_code == 120:
        print("Error: unbalanced parentheses. (Error code 120)")
    elif error_code == 130:
        print(bcolors.FAIL + "Parse error: incompatible types. (Error code 130)" + bcolors.ENDC)
    elif error_code == 500:
        print(bcolors.FAIL + "Error: IOError: File does not exist. (Error code 500)" + bcolors.ENDC)

def clean(file):
    fList = file.read()
    for l in fList:
        if l == "":
            fList -= l
    print(fList)
    return fList

def cleanTokens(tokens):
    for i in range(len(tokens)):
        try:
            if tokens[i][0] == ' ':
                tokens[i] = tokens[i][1:]
        except:
            continue
    return tokens

def lex(file):
    try:
       file = file.read()
    except:
        printError(100)
        quit()
    tok = ""
    par = 0
    q = 0
    tokens = []
    line = 1
    f = ""
    for char in file:
        tok += char
        #print(tok)

        if tok == " ":
            if q != 0:
                tok = ""
            elif tok[:-2] == ",":
                tok = ""
        elif tok == "int":
            tokens.append(tok)
            tok = ""
        elif tok == "string":
            tokens.append(tok)
            tok = ""
        elif tok[-1] == "=" or tok == "=":
            tokens.append("VALUE")
            tokens.append(tok[:-2])
            tok = ""
        elif tok == "show":
            tokens.append("show")
            tok = ""
            #print("found 'show' expression")
        elif tok == "(":
            if q == 0:
                #print("par open")
                tok = ""
                par += 1
        elif tok == ")" or tok[-1] == ")":
            if q == 0:
                #print("par close")
                if tok[-1] != "\"":
                    tokens.append(tok[:-1])
                tok = ""
                par -= 1
        elif tok == "\"":
            if q == 0:
                tok = ""
                #print("string begin")
                q = 1
        elif tok[-1] == "\"":
            #print("string end")
            q = 0
            tokens.append("STRING")
            tokens.append(tok[:-1])
            tok = ""
        elif tok[-1] == "\n" or tok == "\n":
            #print("endline")
            try:
                if tok[-1] == ";" or tok[-2] == ";":
                    tok = ""
                else:
                    print(bcolors.FAIL + "ERROR: syntax error on line " + str(line) + " expected ';' but got new line." + bcolors.ENDC)
                    quit(10)
            except:
                print(bcolors.FAIL + "ERROR: syntax error on line " + str(line) + " expected ';' but got new line." + bcolors.ENDC)
                quit(10)
                tok = ""
            line += 1
        else:
            if q != 1 and tok[-1] == " ":
                tokens.append(tok[:-1])
        try:
            tokens.append(int(tok))
        except:
            continue
        f = tok
        
    if par != 0:
        printError(120)
        quit(10)

    tokens = cleanTokens(tokens)
    print(tokens)
    return tokens

def parse(tokens):
    vars = []
    vararray = {}
    var_declared = 0
    try:
        for i in range(len(tokens)):
            if tokens[i] == "show":
                i += 1
                if tokens[i] == "STRING":
                    print(tokens[i+1])
                elif tokens[i] != "STRING":
                    for c in vars:
                        if c[0] == tokens[i]:
                            print(str(c[1]))
                        else:
                            continue
            elif tokens[i] == "int":
                i += 1
                vars.append([tokens[i], -1])
            elif tokens[i] == "str":
                i += 1
                vars.append(tokens[i])
                vararray.append((tokens[i], ''))
            elif tokens[i] == "VALUE":
                i += 1
                for k in range(len(vars)):
                    if tokens[i] == vars[k][0]:
                        var_declared = 1
                        i += 1
                        vars[k][1] = int(tokens[i])
                    else:
                        print("Undeclared variable " + str(tokens[i]))
                        quit(10)

            #print("DONE! \nExitting...")
        quit(0)
    except (IOError):
        printError(100)
        quit(10)
        #print(bcolors.FAIL + "An unexpected error occured" + bcolors.ENDC)


def run(file):
    #file = clean(f)
    data = lex(file)
    parse(data)
    file.close()
"""
try:
    file = open(argv[1], "r")
    run(file)
    quit()
except:
    file.close()
    print("")
"""

print("--------------------------------------------------------------\n\n    Copyright (C) Jaco Malan, 2017 (https://www.codelog.tk/)\n  Malice Interpreter v0.0.1 BETA\n\n--------------------------------------------------------------\nPlease enter a file name to run.\n")

while True:
    try:
        filename = input(">>> ")
        if filename[-2:] != ".m":
            filename = filename + ".m"
        filename = open(filename, "r")
        run(filename)
    except (IOError):
        printError(500)
        quit(10)

# Malice

Malice is a new programming language that I am developing in my spare time, the interpreter is in python and I will be updating it allot.
Other than that, there is nothing you need to know at this point. So...Cheers

# At the moment

Malice can:

    - You can assign integer variables and print them: "int x = 3;"
    
    - You can print variables: "show(x);" (outputs 3)
    
    - You can print strings: 'show("xyz");'
